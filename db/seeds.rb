# ruby encoding: utf-8
#
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#
I18n.enforce_available_locales = true

printf "\nSeed initiated! \n\n"

printf "\nCreating users: \n"
User.unscoped.delete_all

user_list = [
  [ 1, 'Jim Smith', 'jim@example.com', 'foobar' ],
  [ 2, 'Tom Standish', 'tom@example.com', 'foobar'],
  [ 3, 'Loren Smith', 'loren@example.com', 'foobar' ],
  [ 4, 'William Cartright', 'william@example.com', 'foobar' ],
  [ 5, 'Paul Evans', 'paul@example.com', 'foobar' ],
  [ 6, 'Susan O''Reilly', 'susan@example.com', 'foobar' ],
  [ 7, 'Paul Huffington', 'paulhuff@example.com', 'foobar' ],
  [ 8, 'Yumi Izakowa', 'yumi@example.com', 'foobar' ],
  [ 9, 'Michael Glass', 'michael@example.com', 'foobar' ],
  [ 10, 'Michelle Kuo', 'michelle@example.com', 'foobar' ]
]

user_list.each do |id, name, email, password|
  user = User.create!( id: id, name: name, email: email, password: password, password_confirmation: password )
  p user.name

  # generate access_token
  api_token = user.api_tokens.create!( id: id )

  printf "  =>api token: %s\n\n", api_token.access_token
end

printf "\nCreating places: \n"
Place.unscoped.delete_all

place_list = [
  [ 1, 'Super Burger' ],
  [ 2, 'Ramen Station' ],
  [ 3, 'Philz Coffee' ],
  [ 4, 'The Frying Griddle' ],
  [ 5, 'Cupcake Universe' ],
]

place_list.each do |id, name|
  place = Place.create!( id: id, name: name)
  p place.name
end

printf "\nCreating friendships: \n"
Friendship.unscoped.delete_all

friendship_list = [
  [ 1, 1, 2 ],
  [ 2, 1, 3 ],
  [ 3, 1, 4 ],
  [ 4, 1, 5 ],
  [ 5, 1, 8 ],
  [ 6, 1, 9 ],
  [ 7, 1, 10 ],
  [ 8, 2, 1 ],
  [ 9, 2, 5 ],
  [ 10, 2, 6 ],
  [ 11, 2, 7 ],
  [ 12, 2, 8 ],
  [ 13, 2, 9 ],
  [ 14, 2, 10 ],
  [ 15, 4, 3 ],
  [ 16, 4, 5 ],
  [ 17, 4, 7 ],
  [ 18, 4, 8 ],
  [ 19, 4, 9 ],
  [ 20, 4, 10 ]
]

friendship_list.each do |id, user_id, friend_id|
  friendship = Friendship.create!(id: id, user_id: user_id, friend_id: friend_id)
  p friendship.id
  printf "  =>relationship: %d -> %d\n\n", friendship.user_id, friendship.friend_id
end

printf "\nCreating checkins: \n"
Checkin.unscoped.delete_all

checkins_list = [
  [ 1, 1, 1 ],
  [ 2, 1, 3 ],
  [ 3, 1, 4 ],
  [ 4, 2, 1 ],
  [ 5, 2, 2 ],
  [ 6, 2, 3 ],
  [ 7, 2, 4 ],
  [ 8, 2, 5 ],
  [ 9, 3, 4 ],
  [ 10, 3, 5 ],
  [ 11, 4, 2 ],
  [ 12, 4, 3 ],
  [ 13, 4, 5 ],
  [ 14, 5, 1 ],
  [ 15, 5, 2 ],
  [ 16, 5, 5 ],
  [ 17, 6, 1 ],
  [ 18, 6, 2 ],
  [ 19, 6, 5 ],
  [ 20, 7, 1 ],
  [ 21, 7, 2 ],
  [ 22, 7, 3 ],
  [ 23, 7, 4 ],
  [ 24, 7, 5 ],
  [ 25, 8, 3 ],
  [ 26, 8, 4 ],
  [ 27, 8, 5 ],
  [ 28, 9, 1 ],
  [ 29, 9, 2 ],
  [ 30, 9, 3 ],
  [ 31, 10, 1 ],
  [ 32, 10, 5 ]
]

checkins_list.each do |id, user_id, place_id|
  checkin = Checkin.create!(id: id, user_id: user_id, place_id: place_id)
  p checkin.id
  printf "  =>relationship: %d -> %d\n\n", checkin.user_id, checkin.place_id
end

printf "\nSeed completed! \n\n"

