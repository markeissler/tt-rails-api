# TT Rails API (Example)

Create a lightweight api for accessing TT data with authentication provided by
an access token.

## Web Access
There is a website attached to this Rails app. Users can reach Home, Help, Places and Users pages. If this were a public API, one might want to at least provide a Home page to explain how to signup for API access. Initially, I added this part with the goal of creating a signup form to add users and generate API keys--instead, I ended up importing the provided SQL with sample data via a seed file.

I also exposed Users and Places urls via the web for initial testing. Obviously, the routes can be removed and the controllers, views, etc., that support all of this can be easily removed as well.

## API Access
The API is exposed through the following URI pattern:

	http://WEBSITE.com/api/v1/REST/PATH

An alternative would be to implement an api subdomain pattern instead:

	http://api.WEBSITE.com/v1/REST/PATH
	
## API Versions
The API is versioned. Only v1 is implemented here but adding additional versions is accomplished by implementing the following pattern:

	app/controllers/api/vX
	
The version is namespaced in routes:

	namespace :api, defaults: { format: 'json' } do
	    namespace :vX do
	      resources :users, shallow: false do
	      ...
          end
        end
     end
      
Where "X" (above) is the version number.

## Access Tokens
Access to the API requires an access token. Tokens are bound to users and stored in the api_tokens table, maintained through the ApiToken model. I think this is a flexible design that allows a user to (potentially) generate multiple access tokens; also, a "role" column could be added to restrict the data/operations that a particular token may have access to.

**Limitation:** As things stand right now, an access token provides a user with carte-blanche access to perform CRUD operations on all objects. This isn't ideal for real life use but no specification was provided to limit access to particular users.

Authentication tokens are sent in the header as the **"X-Access-Token"** field. A **401 "Unauthorized"** error is returned if authentication fails.

**Important:** How do you retrieve an access token? After populating the database via the seed file, each user will have an access token available in the api_tokens table. 

	Grab an access token from the CLI for sqlite3:
	
	>sqlite3 db/development.sqlite3
	sqilte3>.headers on
	sqlite3>select user_id, access_token from api_tokens LIMIT 1;
	user_id|access_token
	2|0e5ee4fb-7145-4af9-9f9e-2bf3dc3a1be3
	

## JSON requests
Access to the API is by json requests only: that means endpoints must be typically accessed with a ".json" format specified but for convenience we make json the default format if an endpoint is specified without a format:

	http://WEBSITE.com/api/v1/users
	http://WEBSITE.com/api/v1/users.json
	
The above URLs are equivalent. On the other hand, if the endpoint is accessed with either ".html" or ".xml" as the format, a **422 "Unprocessable entity"** error is returned.

## JSON responses
The API returns its responses in json format. A consistent error interface has been implemented through an **API Base Controller** which results in error messages including an **"error_key"** and **"error_value"** attributes. No specification was provided as to what would determine the key or value; because an object may carry multiple errors following an operation, the key is currently set to the name of the internal operation (e.g. "on update") and the value is set to the object's errors hash. This design would allow for us to append additional keys and values along the way during processing, plus it also ensures that we return **all error messages**.

## Database and Seed Data
This app was built using sqlite3 as the database engine. It could just as easily be deployed with an alternative SQL backend (e.g. Postgres) by changing the Gemfile.

The provided SQL sample data has been converted to a seed file. The following commands will prepare the database and import the data:

	>rake db:migrate
	>rake db:seed
	
You can clear out and reload all data as well if the seed file is changed:

	>rake db:drop
	>rake db:migrate
	>rake db:seed

## Data Destroy (Dependencies)
Through the _has_many_ and _belongs_to_ associations, a particular user's Checkin and Friendship records are destroyed when the corresponding User record is destroyed. In real life, you might want to keep the Checkin data beyond a User record's lifetime so as not to skew checkin statistics later on.

## Differences from Spec
The primary differences between what was requested and what has been delivered is the addition of basic web-level access (a Home page, Help page, Places page, etc.), along with the handling of access tokens. The original request was for a hard-coded access token generated via command line. The implementation of an api_token model provides a greater level of flexibility and accomplishes the same basic task.

## Not the End
This app provides a basic API implementation. It doesn't attempt to catch and recover from anything but basic error conditions. Also, there is a bug (feature?) in Rails 4.0 where certain CRUD operations will return a 204 "No Content" response when the Content-Type is set to application/json, and the _respond_with_ method is used to return a response in an ApplicationController. This is reportedly being fixed in Rails 4.1 but you could code around it by using the following construct:

	render json: @object
	
I chose not to do do that.

## Requirements
This app was built on Rails 4 and Ruby 2, with a sqlite3 database.

## Testing REST
The _api_test.rb_ file at the root level of the project contains some sample statements.

==
