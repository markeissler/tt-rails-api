class Place < ActiveRecord::Base
  # reformat place names for consistency
  before_save { self.name = name.titleize }

  validates :name, presence: true, length: { maximum: 50 }
end
