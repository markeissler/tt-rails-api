module Api
  module V1
    class UsersController < Api::V1::BaseController
      before_filter :find_user, only: [:update, :destroy]
      respond_to :json

      def index
        respond_with User.all
      end

      def show
        respond_with User.find(params[:id])
      end

      def create
        Rails.logger.debug "Api::V1::UsersController:create()"
        @user = User.new(user_params)
        if @user.save
          respond_with :api, :v1, @user
        else
          render_error("on create", @user.errors)
        end
      end

      def update
        Rails.logger.debug "Api::V1::UsersController:update()"
        if @user.update_attributes(user_params)
          respond_with :api, :v1, @user
        else
          render_error("on update", @user.errors)
        end
      end

      def destroy
        Rails.logger.debug "Api::V1::UsersController:destroy()"
        if @user.destroy
          respond_with :api, :v1, @user
        else
          render_error("on destroy", @user.errors)
        end
      end

    private
      def find_user
        @user = User.find(params[:id])
      end

      def user_params
        params.require(:user).permit(:name, :email, :password, :password_confirmation)
      end
    end
  end
end
