module Api
  module V1
    class BaseController < ApplicationController
      before_filter :require_json
      before_filter :require_access_token

    protected
      def require_json
        respond_to do |format|
          format.json { }
          format.html { head :unprocessable_entity }
          format.xml { head :unprocessable_entity }
        end
      end

      # check for token in header as "X-Access-Token" field; set header with...
      #   -H 'X-Access-Token: 28aedd86-2d30-49bd-9a65-0fb5de0e572a'
      #
      def require_access_token
        token_param = request.headers["X-Access-Token"]
        token = ApiToken.find_by_access_token(token_param)
        head :unauthorized unless token
      end

      def render_error(key, value)
        error = {
          "error_key" => key,
          "error_value" => value
          }
        render json: error.to_json, status: :unprocessable_entity
      end

      #
      # Need to get around the csrf token issues when using json. This is not
      # the best way of doing this as it leaves us wide open to attack. Should
      # at least have an acl for acceptable source domains?
      #
      def verified_request?
        if request.content_type == "application/json"
          true
        else
          super()
        end
      end
    end
  end
end
