module Api
  module V1
    class PlacesController < Api::V1::BaseController
      before_filter :find_place, only: [:update, :destroy]
      respond_to :json

      def index
        respond_with Place.all
      end

      def show
        respond_with Place.find(params[:id])
      end

      def create
        Rails.logger.debug "Api::V1::PlacesController:create()"
        @place = Place.new(place_params)
        if @place.save
          respond_with :api, :v1, @place
        else
          render_error("on create", @place.errors)
        end
      end

      def update
        Rails.logger.debug "Api::V1::PlacesController:update()"
        if @place.update_attributes(place_params)
          respond_with :api, :v1, @place
        else
          render_error("on update", @place.errors)
        end
      end

      def destroy
        Rails.logger.debug "Api::V1::PlacesController:destroy()"
        if @place.destroy
          respond_with :api, :v1, @place
        else
          render_error("on destroy", @place.errors)
        end
      end

    private
      def find_place
        @place = Place.find(params[:id])
      end

      def place_params
        params.require(:place).permit(:name)
      end
    end
  end
end
