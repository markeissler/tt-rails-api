module Api
  module V1
    class CheckinsController < Api::V1::BaseController
      before_filter :find_checkin, only: [:update, :destroy]
      respond_to :json

      def index
        respond_with Checkin.all
      end

      def show
        respond_with Checkin.find(params[:id])
      end

     def create
        Rails.logger.debug "Api::V1::CheckinsController:create()"
        @checkin = Checkin.new(checkin_params)
        if @checkin.save
          respond_with :api, :v1, @checkin
        else
          render_error("on create", @checkin.errors)
        end
      end

      def update
        Rails.logger.debug "Api::V1::CheckinsController:update()"
        if @checkin.update_attributes(checkin_params)
          respond_with :api, :v1, @checkin
        else
          render_error("on update", @checkin.errors)
        end
      end

      def destroy
        Rails.logger.debug "Api::V1::CheckinsController:destroy()"
        if @checkin.destroy
          respond_with :api, :v1, @checkin
        else
          render_error("on destroy", @checkin.errors)
        end
      end

    private
      def find_checkin
        @checkin = Checkin.find(params[:id])
      end

      def checkin_params
        params.require(:checkin).permit(:user_id, :place_id)
      end
    end
  end
end
