module Api
  module V1
    class FriendshipsController < Api::V1::BaseController
      before_filter :find_friendship, only: [:update, :destroy]
      respond_to :json

      def index
        respond_with Friendship.all
      end

      def show
        respond_with Friendship.find(params[:id])
      end

     def create
        Rails.logger.debug "Api::V1::FriendshipsController:create()"
        @friendship = Friendship.new(friendship_params)
        if @friendship.save
          respond_with :api, :v1, @friendship
        else
          render_error("on create", @friendship.errors)
        end
      end

      def update
        Rails.logger.debug "Api::V1::FriendshipsController:update()"
        if @friendship.update_attributes(friendship_params)
          respond_with :api, :v1, @friendship
        else
          render_error("on update", @friendship.errors)
        end
      end

      def destroy
        Rails.logger.debug "Api::V1::FriendshipsController:destroy()"
        if @friendship.destroy
          respond_with :api, :v1, @friendship
        else
          render_error("on destroy", @friendship.errors)
        end
      end

    private
      def find_friendship
        @friendship = Friendship.find(params[:id])
      end

      def friendship_params
        params.require(:friendship).permit(:user_id, :friend_id)
      end
    end
  end
end
